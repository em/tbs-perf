#include <gtk/gtk.h>

#include "variable.h"
#include "log.h"

static gboolean verbose = FALSE;
static gint duration = 5;
static gchar **args = NULL;

typedef struct _Size {
    gint width;
    gint height;
    cairo_operator_t operator;
} Size;

typedef struct _Tile {
    cairo_surface_t *surface;
    cairo_operator_t operator;
    gint width;
    gint height;
    gint coord_x;
    gint coord_y;
    gint offset_x;
    gint offset_y;
} Tile;

static Size sizes[] = {
    { 64, 64, CAIRO_OPERATOR_OVER },
    { 64, 64, CAIRO_OPERATOR_SOURCE },
    { 128, 128, CAIRO_OPERATOR_OVER },
    { 128, 128, CAIRO_OPERATOR_SOURCE },
    { 256, 256, CAIRO_OPERATOR_OVER },
    { 256, 256, CAIRO_OPERATOR_SOURCE },
    { 512, 512, CAIRO_OPERATOR_OVER },
    { 512, 512, CAIRO_OPERATOR_SOURCE },
    { 1024, 256, CAIRO_OPERATOR_OVER },
    { 1024, 256, CAIRO_OPERATOR_SOURCE },
    { 1024, 512, CAIRO_OPERATOR_OVER },
    { 1024, 512, CAIRO_OPERATOR_SOURCE },
    { 1024, 1024, CAIRO_OPERATOR_OVER },
    { 1024, 1024, CAIRO_OPERATOR_SOURCE },
    { 2048, 512, CAIRO_OPERATOR_OVER },
    { 2048, 512, CAIRO_OPERATOR_SOURCE },
    { 2048, 1024, CAIRO_OPERATOR_OVER },
    { 2048, 1024, CAIRO_OPERATOR_SOURCE },
    { 2048, 2048, CAIRO_OPERATOR_OVER },
    { 2048, 2048, CAIRO_OPERATOR_SOURCE },
    { -1, -1, CAIRO_OPERATOR_OVER }, // one tile matching the viewport
    { -1, -1, CAIRO_OPERATOR_SOURCE },
    { -1, -4, CAIRO_OPERATOR_OVER }, // tiles 1/4 the viewport height
    { -1, -4, CAIRO_OPERATOR_SOURCE },
    { 0, 0, 0 }
};

static GOptionEntry entries[] =
{
    { "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Be verbose", NULL },
    { "duration", 0, 0, G_OPTION_ARG_INT, &duration, "Duration of a single test, in seconds", "SECONDS" },
    { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &args, NULL, NULL },
    { NULL }
};

static Variable blit = VARIABLE_INIT;
static Tile **tiles = NULL;

static void
window_closed_cb(GtkWidget* widget, GdkEvent* event, gpointer data)
{
    gtk_main_quit();
}

static gboolean
drawing_area_draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data)
{
    Tile *tile;

    gint64 start = g_get_monotonic_time ();
    for (gint i = 0; (tile = tiles[i]) != NULL; i++)
    {
        cairo_set_source_surface(cr, tile->surface, tile->offset_x, tile->offset_y);
        cairo_set_operator (cr, tile->operator);
        cairo_rectangle (cr, tile->offset_x, tile->offset_y, tile->width, tile->height);
        cairo_fill (cr);
    }
    gint64 end = g_get_monotonic_time ();
    variable_add (&blit, end - start);

    gtk_widget_queue_draw (widget);

    return FALSE;
}

static void
print_results (cairo_operator_t operator, gint width, gint height)
{
    g_print ("%4dx%-4d (%6s): (%3d runs) %5.1fµs ± %4.1f max(%5.1f) min(%5.1f)\n", width, height,
             operator == CAIRO_OPERATOR_SOURCE ? "SOURCE" :
             operator == CAIRO_OPERATOR_OVER ? "OVER" :
             "?",
             (int)blit.weight, variable_mean (&blit)/1000, variable_standard_deviation (&blit)/1000,
             blit.max/1000, blit.min/1000);
}

static void
setup_tiles (cairo_operator_t operator, gint tile_width, gint tile_height, gint viewport_width, gint viewport_height)
{
    gint coord_x, coord_y;
    GPtrArray *array;

    array = g_ptr_array_new ();
    for (coord_y = 0; coord_y * tile_height <= viewport_height; coord_y++)
    {
        for (coord_x = 0; coord_x * tile_width <= viewport_width; coord_x++)
        {
            Tile *tile = g_new(Tile, 1);
            tile->width = tile_width;
            tile->height = tile_height;
            tile->coord_x = coord_x;
            tile->coord_y = coord_y;
            tile->offset_x = coord_x * tile_width;
            tile->offset_y = coord_y * tile_height;

            tile->operator = operator;
            tile->surface = cairo_image_surface_create (CAIRO_FORMAT_RGB16_565, tile_width, tile_height);
            cairo_t *cr = cairo_create (tile->surface);
            cairo_set_source_rgb (cr, (coord_x + coord_y) % 2 * 0.2, (coord_x + coord_y) % 2 * 0.6, (1 + coord_x + coord_y) % 2 * 0.6);
            cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
            cairo_rectangle (cr, 0, 0, tile->width, tile->height);
            cairo_fill (cr);

            g_ptr_array_add (array, tile);
        }
    }
    g_ptr_array_add (array, NULL);

    g_free (tiles);
    tiles = (Tile**)array->pdata;

    g_ptr_array_free (array, FALSE);
}

static gboolean                                                                                                         
setup (gpointer data)
{
    gint viewport_width, viewport_height;
    static gint index = 0;
    GtkWidget *drawing_area = data;

    if (index)
        print_results (sizes[index-1].operator, sizes[index-1].width, sizes[index-1].height);

    D("size[%d]: %dx%d", index, sizes[index].width, sizes[index].height);

    if (!sizes[index].width || !sizes[index].height) {
        gtk_main_quit ();
        return FALSE;
    }

    variable_init (&blit);

    viewport_width = gtk_widget_get_allocated_width (drawing_area);
    viewport_height = gtk_widget_get_allocated_height (drawing_area);

    if (sizes[index].width < 0 || sizes[index].height < 0) {
        sizes[index].width = viewport_width / - sizes[index].width;
        sizes[index].height = viewport_height / - sizes[index].height;
    }

    setup_tiles (sizes[index].operator, sizes[index].width, sizes[index].height, viewport_width, viewport_height);

    index++;

    return TRUE;
}

static void
size_allocate_cb (GtkWidget *widget, GdkRectangle *allocation, gpointer user_data)
{
    g_timeout_add_seconds_full (G_PRIORITY_HIGH, duration, setup, widget, NULL);
    setup (widget);

    g_signal_handlers_disconnect_by_func (widget, size_allocate_cb, user_data);
}

int
main(int argc, char** argv)
{
    GError *error = NULL;

    GOptionContext *context = g_option_context_new (NULL);
    g_option_context_add_main_entries (context, entries, NULL);
    g_option_context_add_group (context, gtk_get_option_group (TRUE));

    if (!g_option_context_parse (context, &argc, &argv, &error)) {
        g_printerr ("Error parsing commandline options: %s\n", error->message);
        g_printerr ("Try \"%s --help\" for more information.\n", g_get_prgname ());
        return 1;
    }

    if (verbose)
        log_enable (TRUE);

    GtkWidget* window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_maximize (GTK_WINDOW (window));

    g_signal_connect (window, "delete-event", G_CALLBACK (window_closed_cb), NULL);

    GtkWidget *drawing_area = gtk_drawing_area_new ();
    gtk_container_add (GTK_CONTAINER (window), drawing_area);
    gtk_widget_set_hexpand (drawing_area, TRUE);
    gtk_widget_set_vexpand (drawing_area, TRUE);

    gtk_widget_show_all (window);

    g_signal_connect (drawing_area, "size-allocate",
                      G_CALLBACK(size_allocate_cb), NULL);

    g_signal_connect (drawing_area, "draw",
                      G_CALLBACK(drawing_area_draw_cb), NULL);

    gtk_main ();

    return 0;
}
