#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

static int _log_is_enabled = 0;

static void log_initialize (void) __attribute__((constructor));

static void
log_initialize (void)
{
    _log_is_enabled = getenv (ENVIRONMENT_VARIABLE_NAME) != NULL;
}

int
log_is_enabled (void)
{
    return _log_is_enabled;
}

void
log_enable (int enable)
{
    _log_is_enabled = enable;
}

void
log_debug (const char *format, ...)
{
    va_list ap;

    if (_log_is_enabled) {
        va_start (ap, format);
        vfprintf(stderr, format, ap);
        va_end (ap);
    }
}

void
log_warn (const char *format, ...)
{
    va_list ap;

    va_start (ap, format);
    vfprintf(stderr, format, ap);
    va_end (ap);
}

void
log_error (const char *format, ...)
{
    va_list ap;

    va_start (ap, format);
    vfprintf(stderr, format, ap);
    va_end (ap);

    exit(EXIT_FAILURE);
}

