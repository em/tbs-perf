#ifndef __VARIABLE_H__
#define __VARAIBLE_H__

#include <glib.h>

typedef struct
{
  double weight;
  double sum;
  double sum2;
  double min;
  double max;
  double previous;
} Variable;

#define VARIABLE_INIT { 0.0, 0.0, 0.0, G_MAXDOUBLE, G_MINDOUBLE, G_MINDOUBLE }

void   variable_init               (Variable *variable);
void   variable_add_weighted       (Variable *variable,
                                    double    value,
                                    double    weight);
void   variable_add                (Variable *variable,
                                    double    value);
void   variable_diff               (Variable *variable,
                                    double    value);
double variable_mean               (Variable *variable);
double variable_standard_deviation (Variable *variable);

#endif /* __VARIABLE_H__ */

