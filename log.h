#ifndef __LOG_H__
#define __LOG_H__

#define ENVIRONMENT_VARIABLE_NAME "TBS_PERF_DEBUG"
#define TAG "tbs-perf"

#define D(FORMAT, ...) \
    log_debug(TAG ":%s() [%s:%d]: " FORMAT "\n", \
              __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__)

#define W(FORMAT, ...) \
    log_warn(TAG ":%s() [%s:%d]: " FORMAT "\n", \
             __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__)

#define E(FORMAT, ...) \
    log_error(TAG ":%s() [%s:%d]: " FORMAT "\n", \
              __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__)

int log_is_enabled (void);
void log_enable (int enable);
void log_debug (const char *format, ...);
void log_warn (const char *format, ...);
void log_error (const char *format, ...);

#endif /* __LOG_H__ */
