SOURCES=tbs-perf.c variable.c log.c
HEADERS=variable.h log.h
DEPS=cairo gtk+-3.0
PROGRAM=tbs-perf
PROGRAM_CFLAGS= \
	--std=c1x \
	-Wall \
	-Wcast-align \
	-Wuninitialized \
	-Wno-strict-aliasing \
	-Werror=logical-op \
	-Werror=pointer-arith \
	-Werror=missing-declarations \
	-Werror=redundant-decls \
	-Werror=empty-body \
	-Werror=format \
	-Werror=format-security \
	-Werror=format-nonliteral \
	-Werror=init-self \
	-Werror=vla \
	$(NULL)
PROGRAM_LIBS=`pkg-config --cflags --libs $(DEPS)` -lrt -lm

CC:=cc
CFLAGS:=-O2 -ggdb
PKGCONFIG:=pkg-config

all: $(PROGRAM)

check-deps:
	@$(PKGCONFIG) --cflags --libs $(DEPS) > /dev/null

$(PROGRAM): check-deps $(SOURCES) $(HEADERS) Makefile
	$(CC) $(SOURCES) -o $@ $(PROGRAM_CFLAGS) $(CFLAGS) $(LDFLAGS) $(PROGRAM_LIBS)

clean:
	@rm -f *.o $(PROGRAM)
