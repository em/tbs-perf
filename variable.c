/* -*- mode: C; c-basic-offset: 2; indent-tabs-mode: nil; -*- */

#include <math.h>
#include "variable.h"

void
variable_init (Variable *variable)
{
  variable->weight = 0.0;
  variable->sum = 0.0;
  variable->sum2 = 0.0;
  variable->min = G_MAXDOUBLE;
  variable->max = G_MINDOUBLE;
  variable->previous = G_MINDOUBLE;
}

void
variable_add_weighted (Variable *variable,
                       double    value,
                       double    weight)
{
  variable->weight += weight;
  variable->sum += weight * value;
  variable->sum2 += weight * value * value;
  variable->max = MAX (variable->max, weight * value);
  variable->min = MIN (variable->min, weight * value);
}

void
variable_add (Variable *variable,
              double    value)
{
  variable_add_weighted (variable, value, 1.);
}

void
variable_diff (Variable *variable,
               double    value)
{
  double diff = 0.0;
  if (variable->previous != G_MINDOUBLE)
      diff = value - variable->previous;
  variable->previous = value;

  variable_add_weighted (variable, diff, 1.);
}

double
variable_mean (Variable *variable)
{
  return variable->sum / variable->weight;
}

double
variable_standard_deviation (Variable *variable)
{
  double mean = variable_mean (variable);
  return sqrt (variable->sum2 / variable->weight - mean * mean);
}
